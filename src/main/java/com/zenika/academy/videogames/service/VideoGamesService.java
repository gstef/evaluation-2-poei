package com.zenika.academy.videogames.service;

import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.repository.VideoGamesRepository;
import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class VideoGamesService {

    private VideoGamesRepository videoGamesRepository;
    private RawgDatabaseClient client;

    @Autowired
    public VideoGamesService(VideoGamesRepository videoGamesRepository, RawgDatabaseClient client) {
        this.videoGamesRepository = videoGamesRepository;
        this.client = client;
    }

    public List<VideoGame> ownedVideoGamesWithFilter(String genre) {
        if (genre == null){
            return this.videoGamesRepository.getAll();
        } else {
            return this.videoGamesRepository.getAll()
                    .stream()
                    .filter(vg -> this.isFromGenre(vg, genre))
                    .collect(Collectors.toList());
        }
    }

    private boolean isFromGenre(VideoGame videoGame, String genre){
        return videoGame.getGenres()
                .stream()
                .map(g -> g.getName().equals(genre))
                .reduce((a,b) -> a || b)
                .orElse(false);
    }


    public Optional<VideoGame> getOneVideoGame(Long id) {
        return this.videoGamesRepository.get(id);
    }

    public Optional<VideoGame> addVideoGame(String name) {

        Optional<VideoGame> newGame = this.client.getVideoGameFromName(name);

        newGame.ifPresent(g -> this.videoGamesRepository.save(g));

        return newGame;
    }

    @PostConstruct
    public void addTwoVideoGamesInRepository(){
        this.addVideoGame("Pac-Man");
        this.addVideoGame("Metroid");
    }

    public void deleteVideoGameById(Long id) {
        this.videoGamesRepository.deleteVideoGame(id);
    }

    public void markAsFinishedById(Long id) {
        this.videoGamesRepository.get(id).ifPresent(vg -> vg.setFinished(true));
    }
}
