package com.zenika.academy.videogames.repository;

import com.zenika.academy.videogames.domain.VideoGame;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Component
public class VideoGamesRepository {

    private HashMap<Long, VideoGame> videoGamesById = new HashMap<>();

    public List<VideoGame> getAll() {
        return List.copyOf(this.videoGamesById.values());
    }

    public Optional<VideoGame> get(Long id) {
        VideoGame videoGame = this.videoGamesById.get(id);
        if (videoGame == null) {
            return Optional.empty();
        } else {
            return Optional.of(videoGame);
        }
    }

    public void save(VideoGame v) {
        VideoGame currentVideoGame;
        if ((currentVideoGame = this.videoGamesById.get(v.getId())) != null){
            v.setFinished(currentVideoGame.isFinished());
        }
        this.videoGamesById.put(v.getId(), v);
    }

    public void deleteVideoGame(Long id) {
        this.videoGamesById.remove(id);
    }
}
