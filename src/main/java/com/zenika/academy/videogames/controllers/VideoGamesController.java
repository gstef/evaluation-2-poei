package com.zenika.academy.videogames.controllers;

import com.zenika.academy.videogames.controllers.representation.VideoGameNameRepresentation;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.service.VideoGamesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/video-games")
public class VideoGamesController {

    private VideoGamesService videoGamesService;
    private UtilController utilController;

    @Autowired
    public VideoGamesController(VideoGamesService videoGamesService, UtilController utilController) {
        this.videoGamesService = videoGamesService;
        this.utilController = utilController;
    }

    /**
     * Récupérer la liste des jeux vidéos possédés pas l'utilisateur
     *
     * Exemple :
     *
     * GET /video-games
     */
    @GetMapping
    public List<VideoGame> listOwnedVideoGames(@RequestParam(value = "genre", required = false) String genre ) {
        return this.videoGamesService.ownedVideoGamesWithFilter(genre);
    }

    /**
     * Récupérer un jeu vidéo par son ID
     *
     * Exemple :
     *
     * GET /video-games/3561
     */
    @GetMapping("/{id}")
    public ResponseEntity<VideoGame> getOneVideoGame(@PathVariable("id") Long id) {
        return this.utilController.convert(this.videoGamesService.getOneVideoGame(id));
    }

    /**
     * Ajouter un jeu vidéo à la collection de l'utilisateur
     *
     * Exemple :
     *
     * POST /video-games
     * Content-Type: application/json
     *
     * {
     *     "name": "The Binding of Isaac: Rebirth"
     * }
     */
    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseEntity<VideoGame> addVideoGameByName(@RequestBody VideoGameNameRepresentation videoGameName) {
        return this.utilController.convert(this.videoGamesService.addVideoGame(videoGameName.getName()));
    }

    @DeleteMapping("/{id}")
    public void deleteVideoGameById(@PathVariable("id") Long id){
        this.videoGamesService.deleteVideoGameById(id);
    }

    @PostMapping("/{id}/:finished")
    public void markAsFinishedById(@PathVariable("id") Long id){
        this.videoGamesService.markAsFinishedById(id);
    }
}
