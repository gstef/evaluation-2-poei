package com.zenika.academy.videogames.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UtilController {

    @Autowired
    public UtilController() {
    }

    public <T> ResponseEntity<T> convert(Optional<T> optionalT){
        return optionalT.map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

}

