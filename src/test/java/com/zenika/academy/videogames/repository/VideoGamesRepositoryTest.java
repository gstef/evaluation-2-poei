package com.zenika.academy.videogames.repository;

import com.zenika.academy.videogames.domain.VideoGame;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class VideoGamesRepositoryTest {
    private VideoGamesRepository videoGamesRepository;

    @BeforeEach
    void initializeVideoGameRepository() {
        this.videoGamesRepository = new VideoGamesRepository();
    }

    @Test
    void getAll_onEmptyRepository() {
        List<VideoGame> videoGames = this.videoGamesRepository.getAll();

        assertEquals(List.of(), videoGames); // Should return empty list
    }

    @Test
    void get_onEmptyRepository() {
        assertEquals(Optional.empty(), this.videoGamesRepository.get(0L)); // Should be Optional.empty()
    }

    @Test
    void save_OneVideogameAndGetItBack() {
        VideoGame videoGame = new VideoGame(0L, "test", List.of(), false);
        this.videoGamesRepository.save(videoGame);


        Optional<VideoGame> result = this.videoGamesRepository.get(0L);
        assertAll(() -> assertTrue(result.isPresent()),
                () -> assertEquals(0L, result.get().getId()),
                () -> assertEquals("test", result.get().getName()),
                () -> assertEquals(List.of(), result.get().getGenres()));
    }
}