package com.zenika.academy.videogames.service;

import com.zenika.academy.videogames.domain.Genre;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.repository.VideoGamesRepository;
import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class VideoGamesServiceTest {

    private VideoGamesRepository videoGamesRepository;
    private VideoGamesService videoGamesService;
    private RawgDatabaseClient mockClient;

    @BeforeEach
    void setUp() {
        this.videoGamesRepository = new VideoGamesRepository();
        this.mockClient = mock(RawgDatabaseClient.class);
        this.videoGamesService = new VideoGamesService(this.videoGamesRepository, this.mockClient);
    }

    @Test
    void ownedVideoGames_withOneVideoGame() {
        this.videoGamesRepository.save(new VideoGame(0L, "test", List.of(), false));

        List<VideoGame> videoGames = this.videoGamesService.ownedVideoGamesWithFilter(null);

        assertAll( () -> assertEquals(1, videoGames.size()),
                () -> assertEquals(0L, videoGames.get(0).getId()),
                () -> assertEquals("test", videoGames.get(0).getName()),
                () -> assertEquals(List.of(), videoGames.get(0).getGenres()));
    }

    @Test
    void getOneVideoGame_withOneVideoGame() {
        this.videoGamesRepository.save(new VideoGame(0L, "test", List.of(new Genre("SF")), false));

        Optional<VideoGame> result = this.videoGamesService.getOneVideoGame(0L);

        assertAll(() -> assertTrue(result.isPresent()),
                () -> assertFalse(this.videoGamesService.getOneVideoGame(1L).isPresent()),
                () -> assertEquals(0L, result.get().getId()),
                () -> assertEquals("test", result.get().getName()),
                () -> assertEquals(1, result.get().getGenres().size()),
                () -> assertEquals("SF", result.get().getGenres().get(0).getName()));
    }

    @Test
    void addVideoGame_withEmptyRepository() {
        when(this.mockClient.getVideoGameFromName("Hello"))
                .thenReturn(Optional.of(new VideoGame(1L, "World", List.of(), false)));

        this.videoGamesService.addVideoGame("Hello");
        List<VideoGame> videoGames = this.videoGamesService.ownedVideoGamesWithFilter(null);

        assertAll( () -> assertEquals(1, videoGames.size()),
                () -> assertEquals(1L, videoGames.get(0).getId()),
                () -> assertEquals("World", videoGames.get(0).getName()),
                () -> assertEquals(List.of(), videoGames.get(0).getGenres()));
    }
}